import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 10 * 10";
        assertEquals("100", calculatorResource.calculate(expression));

        expression = " 10 / 2";
        assertEquals("5", calculatorResource.calculate(expression));

        expression = "100+10+10+10";
        assertEquals("130", calculatorResource.calculate(expression));

        expression = "100-10-10-10";
        assertEquals("70", calculatorResource.calculate(expression));

        expression = "2*2*2";
        assertEquals("8", calculatorResource.calculate(expression));

        expression = "100/2/2";
        assertEquals("25", calculatorResource.calculate(expression));

    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100*300";
        assertEquals(30000, calculatorResource.multiplication(expression));

        expression = "10*2";
        assertEquals(20, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10/2";
        assertEquals(5, calculatorResource.division(expression));

        expression = "500/25";
        assertEquals(20, calculatorResource.division(expression));
    }

}
